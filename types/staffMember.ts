export interface StaffMember {
    id: number,
    name: string,
    surname: string,
    date_of_birth: string,
    profession_id: string,
    img: string
}