import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import axios from '~/plugins/axios'
const baseUrl = process.env.BASE_URL;

import { Profession as ProfessionSchema } from '@/types/profession'

@Module({
    name: 'Profession',
    stateFactory: true,
    namespaced: true,
})
export default class Profession extends VuexModule {
    private professions: Array<ProfessionSchema> = [];

    get PROFESSIONS() {
        return this.professions;
    }

    @Mutation
    setProfessions(professions: Array<ProfessionSchema>): void {
        this.professions = professions;
    }

    @Action
    async ADD_PROFESSION(profession: ProfessionSchema): Promise<void> {
        await axios.post(baseUrl + 'api/profession', profession, {
        })
            .catch(err => {
                console.log(err);
            })
    }

    @Action
    async UPDATE_PROFESSION(profession: ProfessionSchema): Promise<void> {
        await axios.put(baseUrl + 'api/profession', profession, {
        })
            .catch(err => {
                console.log(err);
            })
    }

    @Action
    async GET_PROFESSIONS(): Promise<void> {
        const data: any = (await axios.get(baseUrl + 'api/professions')
            .catch(err => {
                console.log(err);
            }))
        this.setProfessions(data.data)
    }

    @Action
    async DELETE_PROFESSION(id: number): Promise<void> {
        await axios.delete(baseUrl + 'api/profession/' + id)
            .catch(err => {
                console.log(err);
            })
    }
}