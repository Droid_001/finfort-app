import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import StaffMember from '~/store/StaffMember'
import Profession from "~/store/Profession"

let staffMemberStore: StaffMember
let professionStore: Profession

const initializer = (store: Store<any>) => {
  staffMemberStore = getModule(StaffMember, store);
  professionStore = getModule(Profession, store);
}

export const plugins = [initializer];

export {
  staffMemberStore,
  professionStore
}
