import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import axios from '~/plugins/axios'
const baseUrl = process.env.BASE_URL;

import { StaffMember as StaffMemberSchema } from '@/types/staffMember'

@Module({
    name: 'StaffMember',
    stateFactory: true,
    namespaced: true,
})
export default class StaffMember extends VuexModule {
    private staffMembers: Array<StaffMemberSchema> = [];

    get STAFF_MEMBERS() {
        return this.staffMembers;
    }

    @Mutation
    setStaffMembers(staffMembers: Array<StaffMemberSchema>): void {
        this.staffMembers = staffMembers;
    }

    @Action
    async ADD_STAFF_MEMBER(staffMember: FormData): Promise<void> {
        await axios.post(baseUrl + 'api/staffMember', staffMember, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
            .catch(err => {
                console.log(err);
            })
    }

    @Action
    async UPDATE_STAFF_MEMBER(staffMember: FormData): Promise<void> {
        await axios.post(baseUrl + 'api/staffMember/update', staffMember, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
        })
            .catch(err => {
                console.log(err);
            })
    }

    @Action
    async GET_STAFF_MEMBERS(): Promise<void> {
        const data: any = (await axios.get(baseUrl + 'api/staffMembers')
            .catch(err => {
                console.log(err);
            }))
        this.setStaffMembers(data.data)
    }

    @Action
    async DELETE_STAFF_MEMBER(id: number): Promise<void> {
        await axios.delete(baseUrl + 'api/staffMember/' + id)
            .catch(err => {
                console.log(err);
            })

    }
}